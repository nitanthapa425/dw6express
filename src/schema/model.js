//defining content of array called

import { model } from "mongoose";
import teacherSchema from "./teacherSchema.js";
import bookSchema from "./bookSchema.js";
import clothSchema from "./clothSchema.js";
import randomSchema from "./randomSchema.js";
import userSchema from "./userSchema.js";
import productSchema from "./productSchema.js";
import reviewSchema from "./reviewSchema.js";

//it is recommended
// to make singular and fistLetter capital of array name
// recommend to make same name of array and model
export let Teacher = model("Teacher", teacherSchema);
export let Book = model("Book", bookSchema);
export let Cloth = model("Cloth", clothSchema);
export let Random = model("Random", randomSchema);
export let User = model("User", userSchema);
export let Product = model("Product", productSchema);
export let Review = model("Review", reviewSchema);
