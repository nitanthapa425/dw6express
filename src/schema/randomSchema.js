// {
//     "name": "c",
//     "age": 33,
//     "password":"Password@123",
//     "phoneNumber": 1111111111,
//     "roll": 2,
//     "isMarried": false,
//     "spouseName": "lkjl",
//     "email": "abc@gmai",
//     "gender": "male",
//     "dob": "2019-2-2",
//     "location": {
//         "country": "nepal",
//         "exactLocation": "gagalphedi"
//     },
// location:{
//     country:{}
// }
//     "favTeacher": [
//         "a",
//         "b",
//         "c",
//         "nitan"
//     ],
//     "favSubject": [
//         {
//             "bookName": "javascript",
//             "bookAuthor": "nitan"
//         },
//         {
//             "bookName": "b",
//             "bookAuthor": "b"

//         }
//     ]
// }
// define the content of object is called Schema
import { Schema } from "mongoose";
let randomSchema = Schema({
  name: {
    type: String,
    //*****************manipulation of data */
    // uppercase: true,
    // lowercase: true,
    // trim:true
    // default: "hari",

    //********************validaation of data */
    required: [true, "name field is required."],
    // minLength: [5, "name must be at least 5 character long"],
    // maxLength: [10, "name mus be at most 10 character."],
  },

  age: {
    type: Number,
    required: [true, "age field is required."],

    //number
    // min: [18, "age must be at least 18"],
    // max: [25, "age must be at most 25"],
  },

  password: {
    type: String,
    required: [true, "password field is required"],
  },
  isMarried: {
    type: Boolean,
    required: [true, "isMarried field is required"],
  },
  roll: {
    type: Number,
    required: [true, "roll field is required"],
  },
  spouseName: {
    type: String,
    required: [true, "spouseName field is required"],
  },
  gmail: {
    type: String,
    required: [true, "gmail field is required"],
  },
  gender: {
    type: String,
    required: [true, "gender field is required"],
  },
  dateOfBirth: {
    type: Date,
    required: [true, "date of birth field is required"],
  },
  location: {
    country: {
      type: String,
      required: [true, "country field is required"],
    },
    exactLocation: {
      type: String,
      required: [true, "exactLocation field is required"],
    },
  },
  favTeacher: [
    {
      type: String,
      required: [true, "favTeacher field is required"],
    },
  ],

  favSubject: [
    {
      bookName: {
        type: String,
        required: [true, "bookName field is required"],
      },
      bookAuthor: {
        type: String,
        required: [true, "bookAuthor field is required"],
      },
    },
  ],
});

export default randomSchema;

// Schema
// manipulation,
// validationa
