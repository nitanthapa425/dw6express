// define the content of object is called Schema
// "location": {
  //         "country": "nepal",
  //         "exactLocation": "gagalphedi"
  //     },
  //     "favTeacher": [
  //         "a",
  //         "b",
  //         "c",
  //         "nitan"
  //     ],
  //     "favSubject": [
  //         {
  //             "bookName": "javascript",
  //             "bookAuthor": "nitan"
  //         },
  //         {
  //             "bookName": "b",
  //             "bookAuthor": "b"
  //         }
  //     ]
  // }
import { Schema } from "mongoose";
let bookSchema = Schema({
  name: {
    type: String,
    required: [true, "name field is required."],
  },

  price: {
    type: Number,
    required: [true, "price field is required."],
  },

  isAvailable: {
    type: Boolean,
    required: [true, "isAvailable field is required"],
  },
  author: {
    type: String,
    required: [false],
  },
});

export default bookSchema;
