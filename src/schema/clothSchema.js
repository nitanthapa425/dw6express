// define the content of object is called Schema
import { Schema } from "mongoose";
let clothSchema = Schema({
  size: {
    type: String,
    required: [true, "size field is required."],
  },

  price: {
    type: Number,
    required: [true, "price field is required."],
  },

  color: {
    type: String,
    required: [true, "color field is required"],
  },
  brand: {
    type: String,
    required: [true, "brand field is required"],
  },
  material: {
    type: String,
    required: [true, "material field is required"],
  },
});

export default clothSchema;
