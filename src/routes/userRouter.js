import { Router } from "express";
import {
  createUser,
  deleteUserById,
  readAllUsers,
  readUserById,
  updateUserById,
} from "../controller/userController.js";
const userRouter = Router();

userRouter.route("/").post(createUser).get(readAllUsers);

userRouter
  .route("/:userId")
  .get(readUserById)
  .patch(updateUserById)
  .delete(deleteUserById);

export default userRouter;
