import { Router } from "express";
import {
  createRandom,
  deleteRandomById,
  readAllRandoms,
  readRandomById,
  updateRandomById,
} from "../controller/randomController.js";
import validation from "../middleware/validation.js";
import Joi from "joi";
import randomValidation from "../validation/randomValidation.js";

const randomRouter = Router();

//Random.create(data)
//Random.find({})
//Random.findById(id)
//Random.findByIdAndDelete(id)
//Random.findByIdAndUpdate(id, data)

//

randomRouter
  .route("/") //localhost:8000/randoms
  .post(validation(randomValidation), createRandom)

  .get(readAllRandoms);

randomRouter
  .route("/:randomId")
  .get(readRandomById)
  .patch(updateRandomById)
  .delete(deleteRandomById);

export default randomRouter;
