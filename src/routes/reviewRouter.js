import { Router } from "express";
import {
  createReview,
  deleteReviewById,
  readAllReviews,
  readReviewById,
  updateReviewById,
} from "../controller/reviewController.js";

const reviewRouter = Router();

reviewRouter.route("/").post(createReview).get(readAllReviews);

reviewRouter
  .route("/:reviewId")
  .get(readReviewById)
  .patch(updateReviewById)
  .delete(deleteReviewById);

export default reviewRouter;
