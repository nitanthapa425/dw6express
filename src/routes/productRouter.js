import { Router } from "express";
import {
  createProduct,
  deleteProductById,
  readAllProducts,
  readProductById,
  updateProductById,
} from "../controller/productController.js";
const productRouter = Router();

productRouter.route("/").post(createProduct).get(readAllProducts);

productRouter
  .route("/:productId")
  .get(readProductById)
  .patch(updateProductById)
  .delete(deleteProductById);

export default productRouter;
