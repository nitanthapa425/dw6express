import { Router } from "express";
import {
  createTeacher,
  deleteTeacherById,
  readAllTeachers,
  readTeacherById,
  updateTeacherById,
} from "../controller/teacherController.js";
const teacherRouter = Router();

//Teacher.create(data)
//Teacher.find({})
//Teacher.findById(id)
//Teacher.findByIdAndDelete(id)
//Teacher.findByIdAndUpdate(id, data)

teacherRouter
  .route("/") //localhost:8000/teachers
  .post(createTeacher)
  .get(readAllTeachers);

teacherRouter
  .route("/:teacherId")
  .get(readTeacherById)
  .patch(updateTeacherById)
  .delete(deleteTeacherById);

export default teacherRouter;
