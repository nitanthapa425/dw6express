import { Router } from "express";
import {
  createCloth,
  deleteClothById,
  readAllCloths,
  readClothById,
  updateClothById,
} from "../controller/clothController.js";

const clothRouter = Router();

clothRouter
  .route("/") //localhost:8000/cloths
  .post(createCloth)
  .get(readAllCloths);

clothRouter
  .route("/:clothId")
  .get(readClothById)
  .patch(updateClothById)
  .delete(deleteClothById);

export default clothRouter;
