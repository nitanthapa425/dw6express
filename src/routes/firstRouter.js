import { Router } from "express";
export let firstRouter = Router();

firstRouter
  .route("/") //localhost:8000
  .post((req, res) => {
    res.json("home post");
  })
  .get((req, res) => {
    res.json("home get");
  })
  .patch((req, res) => {
    res.json("home patch");
  })
  .delete((req, res) => {
    res.json("home delete");
  });

firstRouter
  .route("/name") //localhost:8000/name
  .post((req, res) => {
    res.json("name post");
  })
  .get((req, res) => {
    res.json("name get");
  })
  .patch((req, res) => {
    res.json("name patch");
  })
  .delete((req, res) => {
    res.json("name delete");
  });
