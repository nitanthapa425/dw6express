import Joi from "joi";

let randomValidation = Joi.object()
  .keys({
    name: Joi.string().required(),
    age: Joi.number().required(),
    gender: Joi.string().required(),
    isMarried: Joi.boolean().required(),
    roll: Joi.number().required(),
    gmail: Joi.string().required(),
    password: Joi.string().required(),
    dateOfBirth: Joi.string().required(),
    location: Joi.object().keys({
      country: Joi.string().required(),
      exactLocation: Joi.string().required(),
    }),
    favTeacher: Joi.array().items(Joi.string().required()),
    favSubject: Joi.array().items(
      Joi.object().keys({
        bookName: Joi.string().required(),
        bookAuthor: Joi.string().required(),
      })
    ),
  })
  .unknown(true);

export default randomValidation;
