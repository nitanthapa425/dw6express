import { Random } from "../schema/model.js";

export const createRandom = async (req, res) => {
  let data = req.body;
  //save data to Randoms array

  try {
    let result = await Random.create(data);
    res.json({
      success: true,
      message: "Random created successfully.",
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const readAllRandoms = async (req, res) => {
  try {
    // let results = await Random.find({}); // gives all data
    // let results = await Random.find({ name: "chimmi" }); // gives those object whose name is nitan

    //array searching
    //object searching

    // let results = await Random.find({
    //   name: "nitan",
    // });

    // array searching
    // let results = await Random.find({
    //   favTeacher: "sandeep",
    // });

    //object searching
    // if there is . at key you must wrap key by double quotes
    // let results = await Random.find({
    //   "location.country": "finland",
    // });

    //array of object searching

    // let results = await Random.find({
    //   "favSubject.bookName": "c",
    // });

    //regex searching
    // string " ...."
    // regex /..../
    // let results = await Random.find({
    //   name: /chh/,
    // });
    // starts with
    // let results = await Random.find({
    //   name: /^chh/,
    // });
    //ends with

    //
    // let results = await Random.find({
    //   name: /^nit.*nit$/,
    // });

    //find those name whose name whose contain atleast one @ and _
    // let results = await Random.find({
    //   name: /^(?=.*@)(?=.*_).*$/,
    // });

    // search those name which includes at least number and alphebet
    // let results = await Random.find({
    //   name: /^(?=.*[A-Za-z])(?=.*\d).+$/,
    // });

    //find those name whose name whose length is exactly 3
    // let results = await Random.find({
    //   name: /^\b\w{3}\b/,
    // });

    //find those name whose contain either nitan or ram
    // let results = await Random.find({
    //   name: /\b(?:nitan|ram)\b/,
    // });

    // find those show bookName startst with sic
    // and  bookAuther ends with ees

    // let results = await Random.find({
    //   $and: [
    //     { "favSubject.bookName": /^c/ },
    //     { "favSubject.bookAuthor": /an$/ },
    //   ],
    // });

    // for date searchings
    // 2001-03-12

    // let results = await Random.find({
    //   dateOfBirth: "2001-03-12",
    // });

    // find has power to select object
    // select has power to select object property
    // at select use either
    //   .select("age password")
    //called inclusive it means only show age and password (but _id will show by default to remove use .select("age password -_id"))
    // or .select("-age -password") //called exclusive
    // but dont use like .select("-age password") (dont use exclusive and inclusive at same time) but you can use -_id at any time
    // let results = await Random.find({ name: "hari" }).select(
    //   "-age -password -_id "
    // );

    //for sorting
    // let results = await Random.find({ }).sort("name")
    // let results = await Random.find({ }).sort("-name")//- is for descending sort
    // let results = await Random.find({ }).sort("name age")
    // let results = await Random.find({ }).sort("name -age")

    //for pagination
    // let results = await Random.find({ }).skip("2")
    // let results = await Random.find({}).limit("2");
    // let results = await Random.find({}).skip("2").limit("2");

    // let results = await Random.find({});
    let results = await Random.find({
      name: req.query.name,
      age: req.query.age,
    }).sort(req.query.sort);

    res.json({
      success: true,
      message: "random data read successfully.",
      data: results,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const readRandomById = async (req, res) => {
  let randomId = req.params.randomId;

  try {
    let result = await Random.findById(randomId);
    res.json({
      success: true,
      message: "random read successfully.",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const updateRandomById = async (req, res) => {
  let randomId = req.params.randomId;
  let data = req.body;

  try {
    let result = await Random.findByIdAndUpdate(randomId, data, {
      new: true,
    });

    res.json({
      success: true,
      message: "Random updated successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const deleteRandomById = async (req, res) => {
  let randomId = req.params.randomId;

  try {
    let result = await Random.findByIdAndDelete(randomId);

    res.json({
      success: true,
      message: "Random deleted successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
