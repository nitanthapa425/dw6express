import { Teacher } from "../schema/model.js";

export const createTeacher = async (req, res) => {
  let data = req.body;
  //save data to Teachers array

  try {
    let result = await Teacher.create(data);
    res.json({
      success: true,
      message: "Teacher created successfully.",
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const readAllTeachers = async (req, res) => {
  try {
    let results = await Teacher.find({}); // gives all data

    res.json({
      success: true,
      message: "teacher data read successfully.",
      data: results,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const readTeacherById = async (req, res) => {
  let teacherId = req.params.teacherId;

  try {
    let result = await Teacher.findById(teacherId);
    res.json({
      success: true,
      message: "teacher read successfully.",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const updateTeacherById = async (req, res) => {
  let teacherId = req.params.teacherId;
  let data = req.body;

  try {
    let result = await Teacher.findByIdAndUpdate(teacherId, data, {
      new: true,
    });

    res.json({
      success: true,
      message: "Teacher updated successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const deleteTeacherById = async (req, res) => {
  let teacherId = req.params.teacherId;

  try {
    let result = await Teacher.findByIdAndDelete(teacherId);

    res.json({
      success: true,
      message: "Teacher deleted successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
