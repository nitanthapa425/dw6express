import { Review } from "../schema/model.js";

export const createReview = async (req, res) => {
  let data = req.body;
  //save data to Reviews array

  try {
    let result = await Review.create(data);
    res.json({
      success: true,
      message: "Review created successfully.",
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const readAllReviews = async (req, res) => {
  try {
    let results = await Review.find({})
      .populate("userId", "email password -_id")
      .populate("productId", "name -_id"); // gives all data

    res.json({
      success: true,
      message: "review data read successfully.",
      data: results,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const readReviewById = async (req, res) => {
  let reviewId = req.params.reviewId;

  try {
    let result = await Review.findById(reviewId);
    res.json({
      success: true,
      message: "review read successfully.",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const updateReviewById = async (req, res) => {
  let reviewId = req.params.reviewId;
  let data = req.body;

  try {
    let result = await Review.findByIdAndUpdate(reviewId, data, {
      new: true,
    });

    res.json({
      success: true,
      message: "Review updated successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const deleteReviewById = async (req, res) => {
  let reviewId = req.params.reviewId;

  try {
    let result = await Review.findByIdAndDelete(reviewId);

    res.json({
      success: true,
      message: "Review deleted successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
