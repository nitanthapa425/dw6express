import { Product } from "../schema/model.js";

export const createProduct = async (req, res) => {
  let data = req.body;
  //save data to Products array

  try {
    let result = await Product.create(data);
    res.json({
      success: true,
      message: "Product created successfully.",
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const readAllProducts = async (req, res) => {
  try {
    let results = await Product.find({}); // gives all data

    res.json({
      success: true,
      message: "product data read successfully.",
      data: results,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const readProductById = async (req, res) => {
  let productId = req.params.productId;

  try {
    let result = await Product.findById(productId);
    res.json({
      success: true,
      message: "product read successfully.",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const updateProductById = async (req, res) => {
  let productId = req.params.productId;
  let data = req.body;

  try {
    let result = await Product.findByIdAndUpdate(productId, data, {
      new: true,
    });

    res.json({
      success: true,
      message: "Product updated successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const deleteProductById = async (req, res) => {
  let productId = req.params.productId;

  try {
    let result = await Product.findByIdAndDelete(productId);

    res.json({
      success: true,
      message: "Product deleted successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
