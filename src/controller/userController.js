import { User } from "../schema/model.js";
import { sendMail } from "../utils/sendmail.js";

export const createUser = async (req, res) => {
  let data = req.body;
  //save data to Users array

  try {
    let result = await User.create(data);

    await sendMail({
      from: "'Houseobjob'<uniquekc425@gmail.com>",
      to: [req.body.email],
      subject: "account create",
      html: `
      <h1> your account has been created successfully</h1>
      `,
    });

    res.json({
      success: true,
      message: "User created successfully.",
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const readAllUsers = async (req, res) => {
  try {
    let results = await User.find({}); // gives all data

    res.json({
      success: true,
      message: "user data read successfully.",
      data: results,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const readUserById = async (req, res) => {
  let userId = req.params.userId;

  try {
    let result = await User.findById(userId);
    res.json({
      success: true,
      message: "user read successfully.",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const updateUserById = async (req, res) => {
  let userId = req.params.userId;
  let data = req.body;

  try {
    let result = await User.findByIdAndUpdate(userId, data, {
      new: true,
    });

    res.json({
      success: true,
      message: "User updated successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const deleteUserById = async (req, res) => {
  let userId = req.params.userId;

  try {
    let result = await User.findByIdAndDelete(userId);

    res.json({
      success: true,
      message: "User deleted successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
