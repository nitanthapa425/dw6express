import express, { json } from "express";
import { port } from "./src/constant.js";
import { firstRouter } from "./src/routes/firstRouter.js";
import { foodRouter } from "./src/routes/foodRouter.js";
import connectDb from "./src/connectdb/connectmongodb.js";
import teacherRouter from "./src/routes/teacherRouter.js";
import clothRouter from "./src/routes/clothRouter.js";
import randomRouter from "./src/routes/randomRouter.js";
import userRouter from "./src/routes/userRouter.js";
import productRouter from "./src/routes/productRouter.js";
import reviewRouter from "./src/routes/reviewRouter.js";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { sendMail } from "./src/utils/sendmail.js";
import { config } from "dotenv";

//make expressApp
let expressApp = express();
expressApp.use(json());
config();

console.log(process.env.FULL_NAME);
console.log(process.env.AGE);
console.log(typeof process.env.IS_MARRIED);

connectDb();

// expressApp.use((req, res, next) => {
//   console.log("*****************************");
//   next();
// });

//attached port to that expressApp
expressApp.listen(port, () => {
  console.log(`express app is listening at port port ${port}`);
});

expressApp.use("/", firstRouter); //localhost:8000
expressApp.use("/food", foodRouter); //localhost:8000/food
expressApp.use("/teachers", teacherRouter); //localhost:8000/food
expressApp.use("/cloths", clothRouter); //localhost:8000/food
expressApp.use("/random", randomRouter); //localhost:8000/food
expressApp.use("/users", userRouter); //localhost:8000/food
expressApp.use("/products", productRouter); //localhost:8000/food
expressApp.use("/reviews", reviewRouter); //localhost:8000/food

// url=localhost:8000,
// methode = POST
//  at response "home post"

// 			url=localhost:8000,get at response "home get"
// 			url=localhost:8000,patch at response "home patch"
// 			url=localhost:8000,delete at response "home delete"

//

// let password = "Password@1234";

// // //generate hash code
// let hashCode = await bcrypt.hash(password, 10);
// console.log(hashCode);

// let hashCode = "$2b$10$cd9wJ/O7zQcZbMTjaqpv7uaU.Nob79idLKHBgpeTxVfMvb9srYhcG";

// let isValidpassword = await bcrypt.compare("Password@123", hashCode);

// console.log(isValidpassword);

// let hashCode = await bcrypt.hash(password, 10);
// / let isValidpassword = await bcrypt.compare("Password@123", hashCode);

// jwt ->json web token
// let infoObj = {
//   name: "nitan",
//   _id: "12342134",
// };
// let secretKey = "express6";

// let expiryInfo = {
//   expiresIn: "365d",
// };

// let token = await jwt.sign(infoObj, secretKey, expiryInfo);
// console.log(token);

//verify token
// let token =
//   "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoibml0YW4iLCJfaWQiOiIxMjM0MjEzNCIsImlhdCI6MTY5MzQ4MjkzOSwiZXhwIjoxNzI1MDE4OTM5fQ.WuVFSa9kWj_xX26qRGX0vUxlkRoHMaqZKR7e0cDkru0";

// let infoObj = await jwt.verify(token, "express6");
// console.log(infoObj);

// let fun1 = ()=>{}

// let fun2 = async()=>{}

// fun1()

// await fun2()

// await sendMail({
//   from: "'Houseobjob'<uniquekc425@gmail.com>",
//   to: ["nitanthapa425@gmail.com", "sandipoli010@gmail.com"],
//   subject: "account create",
//   html: `
//   <h1> your account has been created successfully</h1>
//   `,
// });
